var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var val = 0;
var valRed = 0;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
     val = 0;
     valRed = 0;
  console.log('a user connected');
  socket.on('teamBlue', function(msg){
    val++;
    io.emit('teamBlue', val);
  });
  socket.on('teamRed', function(msg){
    valRed++;
    io.emit('teamRed', valRed);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});